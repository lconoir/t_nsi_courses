___
!!! question "Activité 1"

Implémenter une fonction `minimum`, permettant de trouver le minimum d'une liste de façon récursive.

```python
fontion minimum(liste) :
	si la liste est vide :
		renvoyer RIEN
	si la liste comporte un élément :
		renvoyer cet élément
	sinon :
		séparer la liste en 2, liste1 et liste2
		appeler la fonction minimum pour liste1 et mettre le résultat dans la variable min1
		appeler la fonction minimum pour liste2 et mettre le résultat dans la variable min2
		comparer min1 et min2 pour renvoyer le plus petit
```

___
!!! question "Activité 2"

Implémenter une fonction de `tri_fusion`, permettant de trier une liste de façon récursive.

!!! example "Exemple du tri_fusion"
    ```python liste = [24, 15, 10, 3, 14, 58, 2] ```

    ![tri fusion](images/tri_fusion.png)