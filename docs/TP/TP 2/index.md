___
!!! question "Activité 1"

![graphe](graphe_exo1.png)

Implémenter le graphe en langage Python (sous forme de dictionnaire ou de liste d'adjacence).

___
!!! question "Activité 2 : Parcours en largeur"

Écrire une fonction `parcours_largeur` renvoyant le parcours en largeur du graphe, sous forme d'une variable de type `str`.

___
!!! question "Activité 3 : Parcours en profondeur"

Écrire une fonction `parcours_profondeur` renvoyant le parcours en profondeur du graphe, sous forme d'une variable de type `str`.

*NB : Vous pouvez vous servir des implémentations de piles et files fournies en annexe.*