___
## Adresse IP

Déterminer l'adresse IP locale de votre machine.

!!! tip "Commande reseau"
    === "linux"
        - Ouvrir un terminal
        - Taper la commande `ip a`
    === "fenetre"
        - Touches ++"Window"+"R"++
        - Taper "cmd"
        - Taper la commande `ipconfig /all`
    === "pomme"
        - Ouvrir un terminal
        - Taper la commande `ifconfig`

Repérer l'adresse.

![shell fenetre](images/shell.png)



___
## Client - Serveur

1. Avec un binome, décider l'affectation des termes client et serveur pour vos 2 machines :

	Alice et Bob se mettent d'accord.

    Alice : machine serveur, 	Alice utilise le module [`serveur.py`](serveur.py){: target=blank}.

    Bob : machine client,      Bob utilise le module [`client.py`](client.py){: target=blank}.

2. Dans les 2 modules, renseigner l'adresse du serveur puis exécuter les scripts.


___
## Application serveur autonome

1. Côté serveur, enregistrer une nouvelle version du code sous le nom : `serveur_autonome.py`

    !!! tip "Imaginer le serveur d'un restaurant."

        Il propose au client sa carte de menu :

        - entrées  
        - plats  
        - desserts
        
        Le client choisi.

2. Dans ce module, modifier le code de telle manière que :

    - le serveur propose le menu des entrées (que vous définirez) et attend le choix du client
	- le client choisi et lui envoie sa réponse
	- le serveur propose ensuite le menu des plats (idem) et attend le choix du client
	- le client choisi et lui envoie sa réponse
	- le serveur propose le menu des desserts (idem) et attend le choix du client
	- le client choisi et lui envoie sa réponse
	- le serveur récapitule le menu complet et attend la confimation du client
	- le client valide (ou pas) et envoie sa réponse
	- le serveur affiche :
	    "commande en cours" si le client a validé
    	"commande annulée" dans le cas contraire
	- le serveur clore la connexion


___
## Pour aller plus loin

Adapter le jeu du Tic-tac-toe pour un jeu en réseau.
