def moyenne(tableau):
    somme = 0
    nombre = 0
    for element in tableau:
        somme = somme + element
        nombre = nombre + 1
    return somme / nombre


print('moyenne([1.0])')
print(moyenne([1.0]))
print()

print('moyenne([1.0, 2.0, 4.0])')
print(moyenne([1.0, 2.0, 4.0]))
