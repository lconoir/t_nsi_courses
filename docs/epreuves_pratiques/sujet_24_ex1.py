def recherche(elt, tab):
    resultat = -1
    
    for indice in range(len(tab)):
        if tab[indice] == elt:
            resultat = indice
        
    return resultat


print('recherche(1,[2,3,4]')
print(recherche(1,[2,3,4]))
print()

print('recherche(1,[10,12,1,56])')
print(recherche(1,[10,12,1,56]))
print()

print('recherche(1,[1,50,1])')
print(recherche(1,[1,50,1]))
print()

print('recherche(1,[8,1,10,1,7,1,8])')
print(recherche(1,[8,1,10,1,7,1,8]))