def recherche(tab):
    
    tab_consecutifs = []         # initialisation du tableau des couples d'entiers consécutifs
    
    for indice in range(len(tab) - 1):                       # parcourt des indices en s'arrêtant à l'avant-dernier
        
        valeur1, valeur2 = tab[indice], tab[indice + 1]      # 2 valeurs successives dans tab
        
        if valeur1 + 1 == valeur2 :                          # si les 2 valeurs sont consécutives
            tab_consecutifs.append((valeur1, valeur2))
    
    return tab_consecutifs


print("recherche([1, 4, 3, 5])")
print(recherche([1, 4, 3, 5]))
print()

print("recherche([1, 4, 5, 3])")
print(recherche([1, 4, 5, 3]))
print()

print("recherche([7, 1, 2, 5, 3, 4])")
print(recherche([7, 1, 2, 5, 3, 4]))
print()


print("recherche([5, 1, 2, 3, 8, -5, -4, 7])")
print(recherche([5, 1, 2, 3, 8, -5, -4, 7]))

