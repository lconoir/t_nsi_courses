def occurrences(phrase):
    dico = {}
    
    for caractere in phrase :
        if caractere in dico.keys():
            dico[caractere] = dico[caractere] + 1
        else :
            dico[caractere] = 1
            
    return dico