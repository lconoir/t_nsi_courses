# Listes des sujets préparés

## Sujet n°16 : [énoncé](sujets/21_NSI_16.pdf){target=_blank}

=== "Exercice 1"

	````python
	--8<--- "docs/epreuves_pratiques/sujet_16_ex1.py"
	````

=== "Exercice 2"

	````python
	--8<--- "docs/epreuves_pratiques/sujet_16_ex2.py"
	````

## Sujet n°20 : [énoncé](sujets/21_NSI_20.pdf){target=_blank}

=== "Exercice 1"

	````python
	--8<--- "docs/epreuves_pratiques/sujet_20_ex1.py"
	````

=== "Exercice 2"

	````python
	--8<--- "docs/epreuves_pratiques/sujet_20_ex2.py"
	````

## Sujet n°23 : [énoncé](sujets/21_NSI_23.pdf){target=_blank}

=== "Exercice 1"

	````python
	--8<--- "docs/epreuves_pratiques/sujet_23_ex1.py"
	````

=== "Exercice 2"

	````python
	--8<--- "docs/epreuves_pratiques/sujet_23_ex2.py"
	````

## Sujet n°24 : [énoncé](sujets/21_NSI_24.pdf){target=_blank}

=== "Exercice 1"

	````python
	--8<--- "docs/epreuves_pratiques/sujet_24_ex1.py"
	````

=== "Exercice 2"

	````python
	--8<--- "docs/epreuves_pratiques/sujet_24_ex2.py"
	````

## Sujet n°25 : [énoncé](sujets/21_NSI_25.pdf){target=_blank}

=== "Exercice 1"

	````python
	--8<--- "docs/epreuves_pratiques/sujet_25_ex1.py"
	````

=== "Exercice 2"

	````python
	--8<--- "docs/epreuves_pratiques/sujet_25_ex2.py"
	````

## Sujet n°30 : [énoncé](sujets/21_NSI_30.pdf){target=_blank}

=== "Exercice 1"

	````python
	--8<--- "docs/epreuves_pratiques/sujet_30_ex1.py"
	````

=== "Exercice 2"

	````python
	--8<--- "docs/epreuves_pratiques/sujet_30_ex2.py"
	````
