def propager(M, i, j, val):
    
    if M[i][j] == 0: # si le pixel vaut 0
        return       # la fonction s'arrête et renvoie None

    M[i][j] = val    # sinon (sous entendu), on met le pixel à la valeur val

    # si le pixel au-dessus fait partie de la composante
    if ((i - 1) >= 0 and M[i - 1][j] == 1):        # on teste s'il existe un pixel au-dessus et s'il vaut 1
        propager(M, i - 1, j, val)                 # on applique le traitement à ce pixel : appel récursif

    # si le pixel au-dessous fait partie de la composante
    if ((i + 1) < len(M) and M[i + 1][j] == 1):    # on teste s'il existe un pixel au-dessous et s'il vaut 1
        propager(M, i + 1, j, val)                 # on applique le traitement à ce pixel : appel récursif

    # si le pixel à gauche fait partie de la composante
    if ((j - 1) >= 0 and M[i][j - 1] == 1):        # on teste s'il existe un pixel à gauche et s'il vaut 1
        propager(M, i, j - 1, val)                 # on applique le traitement à ce pixel : appel récursif

    # si le pixel à droite fait partie de la composante
    if ((j + 1) < len(M[i]) and M[i][j + 1] == 1): # on teste s'il existe un pixel à droite et s'il vaut 1 (correction erreur sujet : len(M[i])
        propager(M, i, j + 1, val)                 # on applique le traitement à ce pixel : appel récursif


M = [[0, 0, 1, 0], [0, 1, 0, 1], [1, 1, 1, 0], [0, 1, 1, 0]]
propager(M, 2, 1, 3)
print("M = [[0, 0, 1, 0], [0, 1, 0, 1], [1, 1, 1, 0], [0, 1, 1, 0]]")
print("propager(M, 2, 1, 3)")
print("M")
print(M)