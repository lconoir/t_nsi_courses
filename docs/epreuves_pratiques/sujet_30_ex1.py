def multiplication(n1, n2):
    resultat = 0
    
    while n1 != 0 and n2 != 0:
        
        if n1 < 0:
            resultat = resultat - n2
            n1 = n1 + 1
        else :
            resultat = resultat + n2
            n1 = n1 - 1
    
    return resultat


def multi_recursive(n1, n2) :    # version récursive
    if n1 == 0 or n2 == 0:
        return 0
    
    if n1 < 0 :
        return multi_recursive(n1 + 1, n2) - n2
    else:
        return multi_recursive(n1 - 1, n2) + n2



print("multiplication(3, 5)")
print(multiplication(3, 5))
print()

print("multiplication(-4, -8)")
print(multiplication(-4, -8))
print()

print("multiplication(-2, 6)")
print(multiplication(-2, 6))
print()

print("multiplication(-2, 0)")
print(multiplication(-2, 0))