# Enseignement de spécialité NSI en terminale
*Mise à disposition des supports des séances de NSI pour le* _**niveau terminale**_*.*

___
## Les cours

Ils portent sur l'ensemble du programme d'enseignement : [officiel sur Eduscol](https://cache.media.eduscol.education.fr/file/SPE8_MENJ_25_7_2019/93/3/spe247_annexe_1158933.pdf){target=_blank}

- Histoire de l'informatique
- Structure de données (programmation objet, listes, piles, files, arbres, graphes)
- Base de données (modèle relationnel, SGBDR, langage SQL)
- Architectures matérielles, systèmes d’exploitation et réseaux (SoC, processus, ordonnancement, protocoles de routage, chiffrements)
- Langages et programmation (notion de programme, paradigmes, récursivité, modularité, gestion des bugs)
- Architectures matérielles et systèmes d’exploitation
- Algorithmique (arbres, graphes, diviser pour régner, prog. dynamique, recherche textuelle)

___
## Les travaux dirigés

On recherche la mise en application de notions expérimentées ou leur appofondissement.
Ils font l'objet d'une notation individuelle.  
Le temps consacré est variable et estimé en fonction de la longueur et de la difficulté des travaux.  
Cela vient bien dire : si le texte du document est court, le temps de réalisation ne l'est pas nécessairement (et vice versa).

!!! summary "Liste des TD"
     
     |Durée indicative|Titre|
     |:----|:----|
     |**TD 1** (3 heures) | |
     |**TD 2** (4 heures) | |
     |**TD 3** (2 heures) | |
     |**TD 4** (4 heures) | |
     |**TD 5** (3 heures) | |
     |**TD 6** (3 heures) | |
     |**TD 7** (3 heures) | |
     |**TD 8** (3 heures) | |

___
## Les travaux pratiques

On recherche la réflexion, la découverte ou la transposition de notions.  
Ils font l'objet d'une notation collective : par groupe de 2 à 3.  
Tout comme les travaux dirigés, le temps de réalisation est variable.

!!! summary "Liste des TP"

     |Durée indicative|Titre|
     |:----|:----|
     |**TP 1** (2 heures) | 
     |**TP 2** (2 heures) | |
     |**TP 3** (3 heures) | |
     |**TP 4** (2 heures) | |
     |**TP 5** (2 heures) | |
     |**TP 6** (4 heures) | |
     |**TP 7** (3 heures) | |
     |**TP 8** (6 heures) | |
     |**TP 9** (4 heures) | |

___
## Les projets

Les élèves s'inscrivent dans une démarche de projet : le cahier des charges fixe l'objectif (la tâche finale), précise certaines contraintes ou libertés.  
Par groupe, les élèves définissent eux-mêmes la répartition des tâches et gèrent les étapes de la réalisation.
