# -*- coding: utf-8 -*-
'''
:Titre : Implémentation d'une pile, version 1 : POO
:Auteur : L. Conoir
:Date : 10/2020
'''

class Pile():        # en anglais : Stack
    
    def __init__(self, capacite):
        'Constructeur de la classe'
        self.__elements = []
        self.__capacite = capacite
    
    
    def Est_Vide(self):
        'Méthode publique, précise si la pile est vide'
        return len(self.__elements) == 0
            
    
    def Est_Pleine(self):
        'Méthode publique, précise si la pile est pleine'
        return len(self.__elements) == self.__capacite
    
    
    def Empiler(self, element):
        'Méthode publique, empile un nouvel element.'
        if not self.Est_Pleine():
            self.__elements = [element] + self.__elements
        else:
            print('MI1 : La pile est pleine.')    # Stack Overflow
    
    def Depiler(self):
        'Méthode publique, dépile le sommet et le renvoie.'
        if not self.Est_Vide() :
            return self.__elements.pop(0)
        else:
            print('MI2 : La pile est vide.')
    
    
    def Nombre_Elements(self):
        "Méthode publique, renvoie le nombre d'éléments dans la pile."
        return len(self.__elements)
    
    
    def Capacite(self):
        'Méthode publique, renvoie la capacité de la pile.'
        return self.__capacite

    def __str__(self):
        affichage = ''
        for element in self.__elements:
            affichage = affichage + str(element) + '\n--\n'
        print('Voici le contenu de la pile :')
        return affichage