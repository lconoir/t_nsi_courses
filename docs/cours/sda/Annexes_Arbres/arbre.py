# -*- coding: utf-8 -*-
'''
:Titre : Implémentation d'un arbre, version 1 : Module de classe Arbre_binaire
:Auteur : L. Conoir
:Date : 11/2020
'''

#########################################################################
class Arbre_binaire() :
    '''On définit la classe d'objet Arbre_binaire permettant de créer des objets
de type arbre binaire construit à partir de :\n
    - la racine de l'arbre (nécessairement)
    - le sous-arbre gauche issu de sa racine (éventuellement)
    - le sous-arbre droit issu de sa racine (éventuellement)'''

    def __init__(self, noeud, SAG = None, SAD = None) :
        '''Constructeur de la classe'''
        self.__noeud = noeud
        self.__SAG = SAG                # SAG : Sous Arbre Gauche
        self.__SAD = SAD                # SAD : Sous Arbre Droite
        
    def set_noeud(self, noeud):
        '''Méthode publique, modifie la valeur du noeud'''
        self.__noeud = noeud
            
    def set_gauche(self, s_arbre) :
        '''Méthode publique, ajoute un sous arbre gauche'''
        self.__SAG = s_arbre
        
    def set_droite(self, s_arbre) :
        '''Méthode publique, ajoute un sous arbre droite'''
        self.__SAD = s_arbre
        
    def get_noeud(self) :
        '''Méthode publique, renvoie le sous arbre gauche'''
        return self.__noeud
        
    def get_gauche(self) :
        '''Méthode publique, renvoie le sous arbre gauche'''
        return self.__SAG
        
    def get_droite(self) :
        '''Méthode publique, renvoie le sous arbre droite'''
        return self.__SAD
        
    def affiche(self):
        '''Méthode publique, permet d'afficher un arbre'''
        if self == None:
            return None
        else :
            return [self.__noeud, Arbre_binaire.affiche(self.__SAG), Arbre_binaire.affiche(self.__SAD)]
        
    def taille(self):
        '''Méthode publique, renvoie la taille de l'arbre'''
        if self == None:                                                        # si on se place après une feuille (pas de SAG et SAD), ou l'arbre est vide
            return 0
        return 1 + Arbre_binaire.taille(self.__SAG) + Arbre_binaire.taille(self.__SAD)     # 1 noeud + taille de son SAG + taille de son SAD
    
    
    def hauteur(self):
        '''Méthode publique, renvoie la hauteur de l'arbre'''
        if self == None:                                                                           # si on se place après une feuille, ou l'arbre est vide
            return 0
        else :
            return 1 + max(Arbre_binaire.hauteur(self.__SAG), Arbre_binaire.hauteur(self.__SAD))   # niveau du noeud + la plus grande hauteur entre le SAG et le SAD
    
    
    def dictionnaire(self, dico = {}):
        '''Méthode publique, renvoie le dictionnaire d'adjacence de l'arbre'''
        if self != None :
            
            dico[self.__noeud] = []         # ajout du noeud actuel comme clé dans le dictionnaire, avec pour valeur une liste vide
            
            if self.__SAG != None :
            
                dico[self.__noeud].append(self.__SAG.__noeud)     # si un SAG existe, ajout de sa racine dans la liste des valeurs du noeud actuel
                Arbre_binaire.dictionnaire(self.__SAG, dico)      # appel récursif sur le SAG
            
            if self.__SAD != None :
                dico[self.__noeud].append(self.__SAD.__noeud)     # si un SAD existe, ajout de sa racine dans la liste des valeurs du noeud actuel
                Arbre_binaire.dictionnaire(self.__SAD, dico)      # appel récursif sur le SAD
        
        return dico
    
    
    def parcours_largeur(self):
        '''Méthode publique, renvoie le parcours en largeur'''
        from file_1 import File
        
        parcours = []
        file = File(self.taille())                 # créer une file
        file.Enfiler(self)                         # enfiler l'arbre dans la file
        
        while not file.Est_Vide():                 # tant que la file n'est pas vide
            arbre = file.Defiler()                       # défiler un arbre de la file
            parcours.append(arbre.get_noeud())           # ajouter sa racine dans le parcours
            SAG, SAD = arbre.get_gauche(), arbre.get_droite()
        
            if SAG != None:                              # si il existe
                file.Enfiler(SAG)                           # enfiler son sous-arbre gauche dans la file
            
            if SAD != None:                              # si il existe
                file.Enfiler(SAD)                           # enfiler son sous-arbre droit dans la file
                
        return parcours
        
    
    def parcours_profondeur_prefixe(self):
        '''Méthode publique, renvoie le parcours en profondeur dans l'ordre préfixe'''
        if self != None :
            return [self.__noeud] + Arbre_binaire.parcours_profondeur_prefixe(self.__SAG) + Arbre_binaire.parcours_profondeur_prefixe(self.__SAD)
        else :    
            return []
        
    def parcours_profondeur_infixe(self):
        '''Méthode publique, renvoie le parcours en profondeur dans l'ordre infixe'''
        if self != None :
            return Arbre_binaire.parcours_profondeur_infixe(self.__SAG) + [self.__noeud] + Arbre_binaire.parcours_profondeur_infixe(self.__SAD)
        else :
            return []
    
    def parcours_profondeur_suffixe(self):
        '''Méthode publique, renvoie le parcours en profondeur dans l'ordre suffix'''
        if self != None :
            return Arbre_binaire.parcours_profondeur_suffixe(self.__SAG) + Arbre_binaire.parcours_profondeur_suffixe(self.__SAD) + [self.__noeud]
        else :
            return []
    
    
# autres versions d'implémentation (simple consultation)
#    def largeur(self, parcours = [], file = None):          # version récursive
#        '''Méthode publique, renvoie le parcours en largeur'''
#        from file_1 import File
#        
#        if file == None:                           # si la file n'est pas définie
#            file = File(self.taille())                 # on la crée
#            
#        if self != None :                          # si l'arbre existe
#            
#            parcours.append(self.__noeud)             # on inclut sa racine dans le parcours
#            
#            if self.__SAG != None :                      # on enfile le SAG et le SAD s'ils existent
#                file.Enfiler(self.__SAG)
#            if self.__SAD != None :
#                file.Enfiler(self.__SAD)
#        
#        if not file.Est_Vide() :                   # si la file n'est pas vide
#            arbre_suivant = file.Defiler()            # on défile l'arbre suivant
#            Arbre_binaire.largeur(arbre_suivant, parcours, file)   # parcours en largeur à partir de l'arbre suivant
#        
#        return parcours
#    
#    
#    
#    def parcours_prefixe(self, parcours = []):
#        '''Méthode publique, renvoie le parcours en profondeur dans l'ordre préfixe'''
#        if self != None :
#            
#            parcours.append(self.__noeud)
#            
#            if self.__SAG != None:
#                Arbre_binaire.parcours_prefixe(self.__SAG, parcours)
#            
#            if self.__SAD != None:
#                Arbre_binaire.parcours_prefixe(self.__SAD, parcours)
#        
#        return parcours
#    
#    
#    
#    def parcours_infixe(self, parcours = []):
#        '''Méthode publique, renvoie le parcours en profondeur dans l'ordre infixe'''
#        if self != None :
#        
#            if self.__SAG != None:
#                Arbre_binaire.parcours_infixe(self.__SAG, parcours)
#            
#            parcours.append(self.__noeud)
#            
#            if self.__SAD != None:
#                Arbre_binaire.parcours_infixe(self.__SAD, parcours)
#        
#        return parcours
#    
#    def parcours_suffixe(self, parcours = []):
#        '''Méthode publique, renvoie le parcours en profondeur dans l'ordre suffixe'''
#        if self != None :
#        
#            if self.__SAG != None:
#                Arbre_binaire.parcours_suffixe(self.__SAG, parcours)
#            
#            if self.__SAD != None:
#                Arbre_binaire.parcours_suffixe(self.__SAD, parcours)
#            
#            parcours.append(self.__noeud)
#        
#        return parcours
#    
#    
#    def parcours_prefixe_pile(self):          # version avec pile (sans récursivité)
#        '''Méthode publique, renvoie le parcours en profondeur dans l'ordre préfixe'''
#        from pile_1 import Pile
#        
#        parcours = []
#        pile = Pile(self.taille())                 # créer une pile
#        pile.Empiler(self)                         # empiler l'arbre dans la pile
#        
#        while not pile.Est_Vide():                 # tant que la pile n'est pas vide
#            arbre = pile.Depiler()                       # dépiler un arbre de la file
#            
#            parcours.append(arbre.get_noeud())           # ajouter sa racine dans le parcours
#            SAG, SAD = arbre.get_gauche(), arbre.get_droite()
#        
#            if SAD != None:                              # si il existe
#                pile.Empiler(SAD)                           # enpiler son sous-arbre droit dans la pile
#            
#            if SAG != None:                              # si il existe
#                pile.Empiler(SAG)                           # enpiler son sous-arbre gauche dans la pile
#                
#        return parcours
#    
#    
#    def parcours_infixe_pile(self):          # version avec pile (sans récursivité)
#        '''Méthode publique, renvoie le parcours en profondeur dans l'ordre infixe'''
#        from pile_1 import Pile
#        
#        parcours = []
#        pile = Pile(self.taille())                 # créer une pile
#        pile.Empiler(self)                         # empiler l'arbre dans la pile
#        
#        while not pile.Est_Vide():                 # tant que la pile n'est pas vide
#            
#            arbre = pile.Depiler()                       # dépiler un arbre de la file
#            SAG, SAD = arbre.get_gauche(), arbre.get_droite()
#            
#            if SAD != None :                             # si un SAD existe et que le noeud de celui-ci n'est pas dans le parcours
#                pile.Empiler(SAD)                                     # enpiler son sous-arbre droit dans la pile
#            
#            if SAG != None and SAG.get_noeud() not in parcours:   # si un SAG existe et que le noeud de celui-ci n'est pas dans le parcours
#                pile.Empiler(arbre)                                   # empiler l'arbre dans la pile
#                pile.Empiler(SAG)                                     # enpiler son sous-arbre gauche dans la pile
#                
#            else :
#                noeud = arbre.get_noeud()                         # sinon si le noeud n'est pas dans le parcours
#                if noeud not in parcours :
#                    parcours.append(noeud)                            # l'ajouter dans le parcours
#            
#        return parcours
#    
#    
#    def parcours_suffixe_pile(self):          # version avec pile (sans récursivité)
#        '''Méthode publique, renvoie le parcours en profondeur dans l'ordre suffixe'''
#        from pile_1 import Pile
#        
#        parcours = []
#        pile = Pile(self.taille())                 # créer une pile
#        pile.Empiler(self)                         # empiler l'arbre dans la pile
#        
#        while not pile.Est_Vide():                 # tant que la pile n'est pas vide
#            arbre = pile.Depiler()                       # dépiler un arbre de la file
#            
#            parcours.append(arbre.get_noeud())           # ajouter sa racine dans le parcours
#            SAG, SAD = arbre.get_gauche(), arbre.get_droite()
#        
#            if SAG != None:                              # si il existe
#                pile.Empiler(SAG)                           # enpiler son sous-arbre gauche dans la pile
#            
#            if SAD != None:                              # si il existe
#                pile.Empiler(SAD)                           # enpiler son sous-arbre droit dans la pile
#                
#        parcours.reverse()                        # retourner l'écriture du parcours
#        return parcours
    
    
#########################################################################################
# Tests unitaires
def serie_tests() :
    '''
:Examples :
>>> arbre10.taille(), arbre9.taille()
(3, 4)
>>> arbre1.taille(), arbre8.taille()
(7, 8)
>>> arbre1.hauteur(), arbre8.hauteur()
(4, 4)
>>> arbre0.hauteur()
5
>>> arbre0.dictionnaire()
{'0': ['1', '8'], '1': ['2', '4'], '2': ['3'], '3': [], '4': ['5', '6'], '5': [], '6': ['7'], '7': [], '8': ['9', '13'], '9': ['10'], '10': ['11', '12'], '11': [], '12': [], '13': ['14', '15'], '14': [], '15': []}
>>> arbre0.parcours_largeur()
['0', '1', '8', '2', '4', '9', '13', '3', '5', '6', '10', '14', '15', '7', '11', '12']
>>> arbre0.parcours_profondeur_prefixe()
['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15']
>>> arbre0.parcours_profondeur_infixe()
['3', '2', '1', '5', '4', '6', '7', '0', '9', '11', '10', '12', '8', '14', '13', '15']
>>> arbre0.parcours_profondeur_suffixe()
['3', '2', '5', '7', '6', '4', '1', '11', '12', '10', '9', '14', '15', '13', '8', '0']
'''

#########################################################################################
# Vérification des tests unitaires ci-dessus
#########################################################################################
if __name__ == '__main__':
    # Exemple de la page 2 #

    arbre2 = Arbre_binaire('2', Arbre_binaire('3'), None)           # Sous arbre gauche de la racine
    arbre6 = Arbre_binaire('6', None, Arbre_binaire('7'))
    arbre4 = Arbre_binaire('4', Arbre_binaire('5'), arbre6)
    arbre1 = Arbre_binaire('1', arbre2, arbre4)

    arbre10 = Arbre_binaire('10', Arbre_binaire('11'), Arbre_binaire('12'))   # Sous arbre droit de la racine
    arbre13 = Arbre_binaire('13', Arbre_binaire('14'), Arbre_binaire('15'))
    arbre9 = Arbre_binaire('9', None, arbre10)
    arbre8 = Arbre_binaire('8', arbre9, arbre13)

    arbre0 = Arbre_binaire('0', arbre1, arbre8)           # déclaration de l'arbre complet

    import doctest
    doctest.testmod()


