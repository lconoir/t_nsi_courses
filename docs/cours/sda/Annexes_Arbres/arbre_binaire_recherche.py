# -*- coding: utf-8 -*-
'''
:Titre : Fonctions pour un Arbre Binaire de Recherche
:Auteur : L. Conoir
:Date : 11/2020
'''

#####################################################################################################
from arbre import Arbre_binaire as AB

ABR_1 = AB('7', AB('4', AB('1', None, AB('3')),AB('6')), AB('16'))

ABR_2 = AB('8', AB('3',AB('1'), AB('6', AB('4'), AB('7'))), AB('10', None, AB('14', AB('13'), None)))


#####################################################################################################
def recherche_cle(arbre, valeur) :
    '''Prédicat qui précise la présence ou non de la clé 'valeur' dans l'arbre.

:param: arbre, type Arbre_binaire, arbre binaire de recherche
:param: valeur, type str, clé à rechercher
:return: type bool, affirmation de présence de la clé
:CU: les paramètres respectent les types
:bord_effect: None

:Examples:
>>> recherche_cle(ABR_1, '4')
True
>>> recherche_cle(ABR_2, '5')
False
>>> recherche_cle(ABR_2, '13')
True
'''
    if arbre != None :
        etiquette = arbre.get_noeud()
        if etiquette == valeur :
            return True
        elif int(valeur) < int(etiquette) :
            return recherche_cle(arbre.get_gauche(), valeur)
        else :
            return recherche_cle(arbre.get_droite(), valeur)
    else :
        return False


#####################################################################################################
def insere_cle(arbre, valeur) :
    '''La fonction insère une clé dans l'arbre binaire de recherche en respectant les propriétés de l'arbre.

:param: arbre, type Arbre_binaire, arbre binaire de recherche
:param: valeur, type str, clé à insérer
:return: None
:CU: les paramètres respectent les types
:bord_effect: la variable 'arbre' est modifée

:Examples:
>>> insere_cle(ABR_1, '8')
>>> ABR_1.parcours_profondeur_infixe()
['1', '3', '4', '6', '7', '8', '16']
>>> insere_cle(ABR_2, '2')
>>> ABR_2.parcours_profondeur_infixe()
['1', '2', '3', '4', '6', '7', '8', '10', '13', '14']
>>> insere_cle(ABR_2, '9')
>>> ABR_2.parcours_profondeur_infixe()
['1', '2', '3', '4', '6', '7', '8', '9', '10', '13', '14']
'''
    if arbre != None :
        
        if int(valeur) <= int(arbre.get_noeud()) :
            if arbre.get_gauche() == None :
                arbre.set_gauche(AB(valeur))
            else :
                insere_cle(arbre.get_gauche(), valeur)
        else :
            if arbre.get_droite() == None :
                arbre.set_droite(AB(valeur))
            else :
                insere_cle(arbre.get_droite(), valeur)
                
    else :
        arbre = AB(valeur)



#####################################################################################################
# Vérification des tests unitaires ci-dessus
#####################################################################################################
if __name__ == '__main__':
    import doctest
    doctest.testmod()
