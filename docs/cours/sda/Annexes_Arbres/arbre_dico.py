# -*- coding: utf-8 -*-
'''
:Titre : Implémentation d'un arbre, version 2 : tuple (racine, dico des noeuds)
:Auteur : L. Conoir
:Date : 10/2020
'''


def creation_arbre(racine):
    '''La fonction crée un arbre en renvoyant un tuple composé de :
  - sa racine
  - un dictionnaire contenant tous les noeuds avec leurs sous-arbres :
          {noeud1: [sous-arbre gauche, sous-arbre droit], noeud2 ...}
:exemple:
>>> creation_arbre(5)
(5, {5: [None, None]})
'''
    return racine, {racine: [None, None]}


def ajout_SAG(arbre, noeud, s_arbre):
    '''La fonction ajoute le sous-arbre s_arbre dans l'arbre au niveau du sous-arbre
de gauche du noeud, si la place est libre.
:exemple:
>>> arbre_1 = (5, {5: [None, 2], 2: [None, None]})
>>> arbre_2 = (3, {3: [1, None], 1: [None, None]})
>>> ajout_SAG(arbre_1, 5, arbre_2)
>>> arbre_1
(5, {5: [3, 2], 2: [None, None], 3: [1, None], 1: [None, None]})
'''
    racine, dico = arbre
    
    if noeud in dico.keys() and dico[noeud][0] == None:  # si le noeud existe et qu'il ne possède pas de SAD
            
            dico[noeud][0] = s_arbre[0]   # déclaration de la racine du sous_arbre comme SAG du noeud de l'arbre
            
            for cle, valeur in s_arbre[1].items():
                dico[cle] = valeur        # ajout des noeuds du sous_arbre dans l'arbre
            
    
def ajout_SAD(arbre, noeud, s_arbre):
    '''La fonction ajoute le sous-arbre s_arbre dans l'arbre au niveau du sous-arbre
de gauche du noeud, si la place est libre.
:exemple:
>>> arbre_1 = (5, {5: [2, None], 2: [None, None]})
>>> arbre_2 = (3, {3: [1, None], 1: [None, None]})
>>> ajout_SAD(arbre_1, 2, arbre_2)
>>> arbre_1
(5, {5: [2, None], 2: [None, 3], 3: [1, None], 1: [None, None]})
'''
    racine, dico = arbre
    
    if noeud in dico.keys() and dico[noeud][1] == None:  # si le noeud existe et qu'il ne possède pas de SAD
            
            dico[noeud][1] = s_arbre[0]   # déclaration de la racine du sous_arbre comme SAD du noeud de l'arbre
            
            for cle, valeur in s_arbre[1].items():
                dico[cle] = valeur        # ajout des noeuds du sous_arbre dans l'arbre
                
                
def taille(arbre):
    '''Renvoie la taille de l'arbre
:exemple:
>>> arbre_1 = (5, {5: [2, None], 2: [None, 3], 3: [1, None], 1: [None, None]})
>>> taille(arbre_1)
4
'''
    if arbre == None :
        return 0
    else:
        return len(arbre[1])    # taille du dictionnaire


def hauteur(arbre):
    '''Renvoie la hauteur de l'arbre
:exemple:
>>> arbre_1 = (5, {5: [2, None], 2: [None, 3], 3: [1, None], 1: [None, None]})
>>> hauteur(arbre_1)
4
'''
    racine, dico = arbre
    
    if racine == None:
        return 0
    else:
        return 1 + max(hauteur((dico[racine][0], dico)), hauteur((dico[racine][1], dico)))
                     #         (  sous-arbre gauche  )           (  sous-arbre droit   )


def parcours_largeur(arbre):
    '''Renvoie le parcours en largeur
:exemple:
>>> parcours_largeur(arbre2)
[5, 2, 6, 1, 3, 9]
'''
    from file_1 import File
    
    file = File(taille(arbre))
    file.Enfiler(arbre)
    parcours = []
    
    while not file.Est_Vide():
        racine, dico = file.Defiler()
        parcours.append(racine)
        
        racine_SAG, racine_SAD = dico[racine][0], dico[racine][1]
    
        if racine_SAG != None:
            file.Enfiler((racine_SAG, dico))
        
        if racine_SAD != None:
            file.Enfiler((racine_SAD, dico))
    
    return parcours


def parcours_prefixe(arbre):
    '''Renvoie le parcours en profondeur dans l'ordre préfixe
:exemple:
>>> parcours_prefixe(arbre2)
[5, 2, 1, 6, 3, 9]
'''
    racine, dico = arbre
    
    if racine == None:
        return []
    
    else :
        racine_SAG, racine_SAD = dico[racine][0], dico[racine][1]
        
        return [racine] + parcours_prefixe((racine_SAG, dico)) + parcours_prefixe((racine_SAD, dico))


def parcours_infixe(arbre):
    '''Renvoie le parcours en profondeur dans l'ordre infixe
:exemple:
>>> parcours_infixe(arbre2)
[1, 2, 5, 3, 6, 9]
'''
    racine, dico = arbre
    
    if racine == None:
        return []
    
    else :
        racine_SAG, racine_SAD = dico[racine][0], dico[racine][1]
        
        return parcours_infixe((racine_SAG, dico)) + [racine] + parcours_infixe((racine_SAD, dico))

    
def parcours_suffixe(arbre):
    '''Renvoie le parcours en profondeur dans l'ordre suffixe
:exemple:
>>> parcours_suffixe(arbre2)
[1, 2, 3, 9, 6, 5]
'''
    racine, dico = arbre
    
    if racine == None:
        return []
    
    else :
        racine_SAG, racine_SAD = dico[racine][0], dico[racine][1]
        
        return parcours_suffixe((racine_SAG, dico)) + parcours_suffixe((racine_SAD, dico)) + [racine]
    


#######################################################
if __name__ == '__main__':
    
    arbre2 = creation_arbre(5)

    ajout_SAG(arbre2, 5, creation_arbre(2))
    ajout_SAG(arbre2, 2, creation_arbre(1))

    ajout_SAD(arbre2, 5, creation_arbre(6))
    ajout_SAG(arbre2, 6, creation_arbre(3))
    ajout_SAD(arbre2, 6, creation_arbre(9))
    
    import doctest
    doctest.testmod()