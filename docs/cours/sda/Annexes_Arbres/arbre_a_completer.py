# -*- coding: utf-8 -*-
'''
:Titre : Module de classe Arbre_binaire
:Auteur : L. Conoir
:Date : 11/2020
'''

#########################################################################
class Arbre_binaire() :
    def __init__(self, noeud, SAG = None, SAD = None) :
        '''Constructeur de la classe'''
        self.__noeud = noeud
        self.__SAG = SAG                # SAG : Sous Arbre Gauche
        self.__SAD = SAD                # SAD : Sous Arbre Droite
        
    def set_noeud(self, noeud):
        '''Méthode publique, modifie la valeur du noeud'''
        self.__noeud = noeud
            
    def set_gauche(self, s_arbre) :
        '''Méthode publique, ajoute un sous arbre gauche'''
        self.__SAG = s_arbre
        
    def set_droite(self, s_arbre) :
        '''Méthode publique, ajoute un sous arbre droite'''
        self.__SAD = s_arbre
        
    def get_noeud(self) :
        '''Méthode publique, revoie le sous arbre gauche'''
        return self.__noeud
        
    def get_gauche(self) :
        '''Méthode publique, revoie le sous arbre gauche'''
        return self.__SAG
        
    def get_droite(self) :
        '''Méthode publique, revoie le sous arbre droite'''
        return self.__SAD
        
    def affiche(self):
        '''Méthode publique, permet d'afficher un arbre'''
        if self == None:
            return None
        else :
            return [self.__noeud, Arbre_binaire.affiche(self.__SAG), Arbre_binaire.affiche(self.__SAD)]
        
    def taille(self):
        '''Méthode publique, renvoie la taille de l'arbre'''
        if self == None:                                                        # si on se place après une feuille (pas de SAG et SAD), ou l'arbre est vide
            return 0
        return 1 + Arbre_binaire.taille(self.__SAG) + Arbre_binaire.taille(self.__SAD)     # 1 noeud + taille de son SAG + taille de son SAD
    
    
    def hauteur(self):
        '''Méthode publique, renvoie la hauteur de l'arbre'''
                                                                    # si on se place après une feuille, ou l'arbre est vide
        
        
                                                                    # niveau du noeud + la plus grande hauteur entre le SAG et le SAD
    
    
   
    
    
# Exemple de la page 2 ###################################################################
feuille3 = Arbre_binaire('3')                       # déclaration des feuilles
feuille5 = Arbre_binaire('5')
feuille7 = Arbre_binaire('7')
feuille11 = Arbre_binaire('11')
feuille12 = Arbre_binaire('12')
feuille14 = Arbre_binaire('14')
feuille15 = Arbre_binaire('15')

arbre2 = Arbre_binaire('2',feuille3,None)           # Sous arbre gauche de la racine
arbre6 = Arbre_binaire('6',None, feuille7)
arbre4 = Arbre_binaire('4',feuille5,arbre6)
arbre1 = Arbre_binaire('1',arbre2,arbre4)

arbre10 = Arbre_binaire('10',feuille11,feuille12)   # Sous arbre droit de la racine
arbre13 = Arbre_binaire('13',feuille14,feuille15)
arbre9 = Arbre_binaire('9',None,arbre10)
arbre8 = Arbre_binaire('8',arbre9,arbre13)

arbre0 = Arbre_binaire('0',arbre1,arbre8)           # déclaration de l'arbre complet



#########################################################################################
# Tests unitaires
def serie_tests() :
    '''
:Examples :
>>> feuille3.taille(), arbre2.taille()
(1, 2)
>>> arbre1.taille(), arbre0.taille()
(7, 16)
>>> feuille3.hauteur(), arbre2.hauteur()
(1, 2)
>>> arbre1.hauteur(), arbre0.hauteur()
(3, 4)

'''

#########################################################################################
# Vérification des tests unitaires ci-dessus
#########################################################################################
if __name__ == '__main__':
    import doctest
    doctest.testmod()


