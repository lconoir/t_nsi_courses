# -*- coding: utf-8 -*-
'''
:Titre : Les graphes _ Algogrithme de Dijkstra
:Auteur : L. Conoir
:Date : 11/2020
'''


# Dictionnaire d'ajacence du graphe
dico_graphe = {'A': {'B': 2, 'G': 2},
               'B': {'A': 2, 'C': 5, 'D': 4},
               'C': {'B': 5, 'E': 2, 'H': 1},
               'D': {'B': 4, 'E': 1, 'G': 3},
               'E': {'C': 2, 'D': 1, 'F': 1, 'G': 2, 'H': 3},
               'F': {'E': 1, 'G': 2},
               'G': {'A': 2, 'D': 3, 'E': 2, 'F': 2},
               'H': {'C': 1, 'E': 3}}

dico_graphe_DS = {'A': {'D': 1, 'E': 2, 'F': 2, 'G': 2},
                  'B': {'D': 2, 'E': 3},
                  'C': {'F': 2, 'G': 1},
                  'D': {'A': 1, 'B': 2, 'E': 2, 'F': 4},
                  'E': {'A': 2, 'B': 3, 'D': 2},
                  'F': {'A': 2, 'C': 2, 'D': 4, 'G': 3},
                  'G': {'A': 2, 'C': 1, 'E': 5, 'F': 3}}
                



def dijkstra(graphe, sommet_depart, bk_liste = []):
    '''La fonction renvoie un dictionnaire dont les clés sont les sommets que l'on peut atteindre à partir du sommet_depart.
A chaque clé est associée la distance minimale qui sépare ce sommet du sommet_depart.

:param: graphe, type dict, dictionnaire d'adjacence du graphe
:param: sommet_depart, type str, sommet à partir duquel sont calculées les distances
:param: bk_liste, type list, liste de sommets à éviter
:return: type dict, dictionnaire des distances par rapport aux sommets
:CU: Respect des types
:bord_effect: None
'''
    file = [sommet_depart]
    distance = {sommet_depart : [0, sommet_depart]}
    
    while file != []:
        parcours = file.pop(0)
        sommet = parcours[-1]
        voisins = graphe[sommet]
        
        for voisin in voisins :
            
            if voisin not in bk_liste :                
                nouvelle_distance = distance[sommet][0] + voisins[voisin]
                
                if voisin not in distance or nouvelle_distance < distance[voisin][0] :
                    distance[voisin] = [nouvelle_distance, parcours + voisin]
                    file.append(parcours + voisin)
                    
    return distance



print("Distances à partir de 'B' : ")
print(str(dijkstra(dico_graphe_DS,'B')))

print("Distances à partir de 'B' (sans A) :")
print(str(dijkstra(dico_graphe,'B', ['A'])))