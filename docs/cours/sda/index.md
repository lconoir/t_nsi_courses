___
Dans certaines situations, le développeur est amené à imaginer et concevoir une structure de données adaptée : une manière d'organiser et de stocker l'information, afin de la traiter plus facilement.

Dans bon nombre de langages, des structures sont prévues nativement comme type de données, mais ce n'est pas toujours le cas.

Des routines (procédures et fonctions) sont généralement associées à une structure : elles constituent son **interface**.

Une interface de base pourrait être définie ainsi (méthode CRUD) :

- **C**  create : ajout d'une donnée
- **R**  read : lecture d'une donnée
- **U**  update : modification d'une donnée
- **D**  delete	: suppression d'une donnée
  
Cela suppose bien entendu qu'une donnée est identifiable et repérable dans la structure, par le biais d'une routine de recherche.

Les structures les plus utilisées sont :

- les structures linéaires :

|**les listes**	| **les files**	| **les piles**|
|:-:|:-:|:-:|
|![image_liste](images/liste.png)|![image_file](images/file.png)|![image_pile](images/pile.png)|

- les structures à accès par clé : **les dictionnaires**

![image_dico](images/dico.png){width=50%}

|les structures hiérarchiques : **les arbres**	| les structures relationnelles : **les graphes**|
|:-:|:-:|
|![image_arbre](images/arbre.png){align=right}|![image_graphe](images/graphe.png){align=right}|