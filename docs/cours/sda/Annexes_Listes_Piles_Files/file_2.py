# -*- coding: utf-8 -*-
'''
:Titre : Implémentation d'une file, version 2 : fonctionnel
:Auteur : L. Conoir
:Date : 10/2020
'''

def Creer_file_Vide(capacite):
    '''La fonction renvoie un dictionnaire comportant :
     une clé 'elements' (liste des éléments) ayant pour valeur une liste vide
     une clé 'nb_elements' (nombre d'éléments) ayant pour valeur 0
     une clé 'capacite' (taille maximale) ayant pour valeur la valeur de la variable capacite
:example :
>>> Creer_file_Vide(10)
{'elements': [], 'nb_elements': 0, 'capacite': 10}
'''
    return {'elements' : [],
            'nb_elements' : 0,
            'capacite' : capacite}           


def Est_Vide(file):
    '''Prédicat signalant si la file est vide.
:example :
>>> file_essai = {'elements': [], 'nb_elements': 0, 'capacite': 10}
>>> Est_Vide(file_essai)
True
>>> file_essai = {'elements': [], 'nb_elements': 1, 'capacite': 10}
>>> Est_Vide(file_essai)
False
'''
    return file['nb_elements'] == 0


def Est_Pleine(file):
    '''Prédicat signalant si la file est pleine.
:example :
>>> file_essai = {'elements': [], 'nb_elements': 10, 'capacite': 10}
>>> Est_Pleine(file_essai)
True
>>> file_essai = {'elements': [], 'nb_elements': 8, 'capacite': 10}
>>> Est_Pleine(file_essai)
False
'''
    return file['nb_elements'] == file['capacite']


def Enfiler(file, element):
    '''La fonction ajoute un element à la fin de la file.
:example :
>>> file_essai = {'elements': [5, 4, 8], 'nb_elements': 3, 'capacite': 10}
>>> Enfiler(file_essai, 6)
>>> file_essai
{'elements': [5, 4, 8, 6], 'nb_elements': 4, 'capacite': 10}
'''
    if not Est_Pleine(file) :
        file['elements'].append(element)
        file['nb_elements'] += 1
    else :
        print('MI : La file est pleine.')


def Defiler(file):
    '''La fonction défile le premier élément, si il existe, et le renvoie.
:example :
>>> file_essai = {'elements': [5, 4, 8], 'nb_elements': 3, 'capacite': 10}
>>> Defiler(file_essai)
5
>>> Defiler(file_essai)
4
>>> file_essai
{'elements': [8], 'nb_elements': 1, 'capacite': 10}
'''
    if not Est_Vide(file) :
        file['nb_elements'] -= 1
        return file['elements'].pop(0)
    else :
        print('MI : La file est vide.')


def Nombre_Element(file):
    '''La fonction renvoie le nombre d'éléments dans la file.'''
    return file['nb_elements']


def Capacite(file):
    '''La fonction renvoie la capacité de la file.
(nombre maximal d'éléments que peut contenir la file)'''
    return file['capacite']




########################################################################
def Creer_file_Vide_2(capacite):
    return [capacite]


def Est_Vide_2(file):
    return len(file) == 1


def Est_Pleine_2(file):
    return len(file) - 1 == file[-1]


def Enfiler_2(file, element):
    if not Est_Pleine(file) :
        capacite = file.pop()
        file.append(element)
        file.append(capacite)
    else :
        print('MI : La file est pleine.')
    
def Defiler_2(file):
    if not Est_Vide(file) :
        return file.pop(0)
    else :
        print('MI : La file est vide.')
        
def Nombre_Element_2(file):
    return len(file) - 1

def Capacite_2(file):
    return file[-1]


###############################################################################
if __name__ == '__main__':
    import doctest
    doctest.testmod()