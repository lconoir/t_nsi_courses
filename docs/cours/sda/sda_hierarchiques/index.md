___
Un **arbre** est un graphe, non orienté, ne possédant pas de cycle.
On retrouve cette structure dans différentes situations où une hiérarchie est établie.

- un cours ou un livre : Titre, Chapitres, Paragraphes
- les langages par balisage comme HTML : imbrication des balises
- la généalogie : ancêtres et descendants
- les organigrammes : celui de l'état, d'une entreprise ...
- l'arborescence des répertoires et fichiers
- ....

!!! example "On peut la représenter de différentes façons"
    |version informatique|autre version|
    |:-:|:-:|
    |![arbre_cours2](images/arbre_cours2.png){width=65%}|![arbre_cours](images/arbre_cours.png){width=70%}|

___
## Représentation en informatique et vocabulaire

La hiérarchie est établie **de haut en bas**.

Les sommets portent des noms différents : ce sont les **nœuds** de l'arbre

La **racine** est le nœud à la tête de la hiérarchie : **A** *(dans cet exemple)*

Les **feuilles** sont les nœuds qui terminent les branches de l'arbre : **B, C, G, H, I, K, L**

!!! note "Autres termes utilisés"
    === "arbres et sous-arbres *(retenu dans le cours)*"
        Un **arbre** est composé de **sous-arbres**  
        L'arbre issu de F est un sous arbre de D  
        L'arbre issu de A est composé 3 sous-arbres
    
    === "parent et enfants"
        Un nœud est un **parent**.  
        Le nœud sans parent est la racine.  
        Un nœud sans **enfant** est une feuille.  

Un arbre a des dimensions :

- une **taille **: le nombre de nœuds qui le compose
- une **hauteur **: le nombre d'arêtes entre sa racine et sa feuille la plus basse  
*(la longueur de sa plus grande branche)*
- une **arité **: le nombre maximal d'enfant pour un noeud

Dans l'exemple, l'arbre a pour dimensions : taille = 12, hauteur = 4, arité = 3

!!! warning "Particularité"
    Par convention, on considère que si un arbre n'a que sa racine, il a une hauteur de 1.

___
## Arbres binaires

Les arbres dont l'arité est inférieure ou égale à 2 sont appelés des **arbres binaires**.

!!! note "Adaptation du vocabulaire"
    Un nœud (autre qu'une feuille) possède alors au maximum un sous-arbre gauche (SAG) et un sous-arbre droit (SAD)  
    *(Autrement dit, un parent possède au maximum un enfant gauche et un enfant droit)*

![arbre_cours4](images/arbre_cours3.png){width=90%}

Implémentation possible d'un arbre en POO (programmation orientée objet) :

On définit la classe d'objet **`Arbre_binaire`** possédant :

- attributs : 
    - **`nœud`** (valeur du nœud) 
    - **`SAG`** (sous-arbre gauche)
    - **`SAD`** (sous-arbre droit)
- méthodes : 
    - **`set_gauche()`**
    - **`set_droit()`**
    - **`get_gauche()`**
    - **`get_droit()`**
    - ...

___
## Parcours en largeur

Suivant le même principe que pour les graphes, le parcours en largeur d'un arbre (noté BFS pour Breadth-First Search) revient à énumérer **les nœuds par ordre hiérarchique** décroissant, de gauche à droite *(lecture classique d'un texte)*

*Exemple précédent :* 0 1 8 2 4 9 13 3 5 6 10 14 15 7 11 12

!!! summary "Algorithme :"
    créer une **file**  
    **enfiler** l'arbre dans la **file**  
    **tant que** la **file** n'est pas vide :  
      - **défiler** un **arbre** de début de file  
      - inclure la racine dans le parcours  
      - si il existe, **enfiler** le sous-arbre de **gauche** dans la file  
      - si il existe, **enfiler** le sous-arbre de **droit** dans la file  
    renvoyer le parcours

___
## Parcours en profondeur

Le parcours en profondeur d'un arbre (noté DPS pour Depth-First Search) peut se présenter de trois manières différentes.

Cela dépend de l'écriture de la valeur du nœud dans le parcours par rapport à l'exploration de ses sous arbres : **préfixe** (avant, pastille bleue), **infixe**  (entre, pastille rouge), **postfixe** (après, pastille verte)

!!! example "Le noeud est écrit dans le parcours lors du passage sur la pastille"
    ![arbre](images/arbre_cours4.png){width=90%}

!!! summary "Algorithmes :"
    |**ordre préfixe**  *(pastille bleue)*|**ordre infixe**  *(pastille rouge)*|**ordre suffixe**  *(pastille verte)*|
    |:-|:-|:-|
    |si l'arbre n'est pas vide :|si l'arbre n'est pas vide :|si l'arbre n'est pas vide :|
    |. . mettre dans le parcours :|. . mettre dans le parcours :|. . mettre dans le parcours :|
    |. . . . le noeud|. . . . le parcours du SAG|. . . . le parcours du SAG|
    |. . . . le parcours du SAG|. . . . le noeud|. . . . le parcours du SAD|
    |. . . . le parcours du SAD|. . . . le parcours du SAD|. . . . le noeud|
    |0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15|3 2 1 5 4 6 7 0 9 11 10 12 8 14 13 15|3 2 5 7 6 4 1 11 12 10 9 14 15 13 8 0|

!!! example "Application à un arbre de calcul""
    ![arbre_calcul](images/arbre_calcul.png){width=50%}

    Ordre préfixe : + × 6 + x y - y 14  
    Langage parlé : **Addition** du **produit** de 6 par l\'**addition** de x et y *avec* la **soustraction** de y avec 14

    Ordre infixe : 6 × x + y + y - 14      
    Odre d'écriture mathématique : 6 × (x + y) + (y - 14)

    Ordre postfixe : 6 x y + × y 14 - +  
    Notation polonaise inversée : les valeurs sont lues et empilées avant la lecture d'un opérateur.

        - On empile les valeurs lues
        - A la lecture d'un opérateur, on dépile deux valeurs pour effectuer l'opération.
        - On empile le résultat.
        - On continue ainsi jusqu'à la fin : il reste un élément dans la pile, le résultat final.

___
## Arbre binaire de recherche (ABR)

Un arbre binaire de recherche est un arbre binaire possédant les propriétés suivantes :

- chaque noeud porte une étiquette : une **clé**
- les clés contenues dans un **sous-arbre gauche ont une valeur inférieure ou égale** à celle de sa racine
- les clés contenues dans un **sous-arbre droit ont une valeur strictement supérieure** à celle de sa racine

*Les arbres binaires de recherche servent, comme leur nom l'indique, à rechercher rapidement des éléments ordonnés.*

D'ailleurs, le parcours en profondeur en ordre infixe, permet d\'avoir les noeuds rangés dans l'ordre croissant de leur valeur.

!!! example "Exemples d'arbre :"
    |Ceux-ci ne possèdent pas les propriétés|Mais en modifiant les emplacements|
    |:-:|:-:|
    |![arbre1](images/abr_exemple1.png){width=80%}|![arbre](images/abr_exemple2.png){width=80%}|
    ||ordre infixe : 1 2 3 4 5 6 7 8 9|
    |![arbre3](images/abr_exemple3.png){width=80%}|![arbre4](images/abr_exemple4.png){width=80%}|
    ||ordre infixe : 3 4 5 8 10 15 20|

Implémentation d'arbre binaire de recherche : ABR

|ABR_1|ABR_2|
|:-:|:-:|
|![abr1](images/ABR_1.png){width=90%}|![abr2](images/ABR_2.png){width=90%}|


```python
from arbre import Arbre_binaire as AB    # l'alias 'AB' fait gagner de la place

ABR_1 = AB('7', AB('4', AB('1', None, AB('3')),AB('6')), AB('16'))

ABR_2 = AB('8', AB('3',AB('1'), AB('6', AB('4'), AB('7'))), AB('10', None, AB('14', AB('13'), None)))
```

### Recherche d'une clé dans un ABR :

!!! summary "Algorithme :"
    fonction recherche_cle avec paramètres arbre et valeur :

        - si l'arbre n'est pas vide :
            -  si la racine est la valeur cherchée, on renvoie Vrai
            - sinon si la valeur est inférieure ou égale à la racine :
                - on recherche la valeur dans le sous-arbre degauche
            - sinon on recherche la valeur dans le sous-arbre de droite
        - sinon, on renvoie Faux

### Insertion d'une clé dans un ABR :

```
fonction insere_cle avec paramètres arbre et valeur :

- si l'arbre n'est pas vide :
      - si la valeur est inférieure ou égale à la racine :
        - si le sous-arbre de gauche est vide, on place une feuille avec la valeur
        - sinon on insère la valeur dans le sous_arbre de gauche
    - sinon
        - si le sous-arbre de droite est vide, on place une feuille avec la valeur
        - sinon on insère la valeur dans le sous_arbre de droite
- sinon on crée un arbre avec noeud contenant la valeur
```