___
## Notion de processus


### Pour commencer : 

Le terme « programme » est souvent utilisé à tord :

> « mon programme ne compile pas »  
> « je copie le programme sur ma clé USB »  
> « j'ai 3 programmes qui s'exécutent »  

Donc, pour être clair :

- **Programme** : séquence d'instructions en langage machine stocké en RAM
- **Image** : fichier contenant l'image mémoire d'un programme
- **Source** : fichier en langage évolué servant à produire une image
- **Code ou Script** : texte utilisé pour écrire le source



### Définition

**Processus** : c'est l'exécution d'un programme

La création d'un processus peut se produire :

- au démarrage du système
- par un autre processus
- par l'action de l'utilisateur

Plusieurs processus peuvent exécuter le même programme.

!!! tip "Analogie à la musique"
    === "programme"
        partition (avec des choix possibles...)
    === "processus"
        musicien (ou plusieurs)
    === "processeur"
        instrument (ou plusieurs)
    === "système d'exploitation"
        chef d'orchestre
    === "utilisateur"
        spectateur (ou plusieurs)

!!! info "Remarques"
    - script, source, image, programme : notions *statiques*
    - processus : notion *dynamique*


NB: les commandes UNIX `ps` et `top`, et le gestionnaire de tâches de Windows (onglet « Processus »), donnent des informations sur les *processus* en cours



### États et vie d'un processus

Selon les systèmes d'exploitation, le nombre et le nom des états peut varier, mais la base commune est :

- **Élu** : en train de s'exécuter sur un processeur
- **Éligible** : en attente d'un processeur pour s'exécuter
- **Bloqué** : en attente d'un événement (ex: interruption, accès ressource...), il ne peut être éligible pour l'instant et encore moins s'exécuter


!!! info "Diagrammes d'états d'un processus"
    === "Version 1"
        ![figure](images/ps_states.png){width = 30%}
    === "Version 2"
        ![figure](images/processus_vie.png){width = 70%}

Un processeur ne s'occupe que d'un processus **élu** _(le processeur n'est pas multi-tâches)_.  
Quand celui-ci passe à l'état **bloqué**, un autre processus peut prendre sa place en devenant l'**élu**.

Un processus peut se terminer pour plusieurs raisons :

- normalement (travail terminé)
- par erreur d'exécution
- par erreur fatale (ressource disparue subitement, mémoire saturée...)
- par arrêt d'un autre processus
- ....


### Interblocage

Un **interblocage** (*deadlock*) est une situation où plusieurs processus sont bloqués car chacun attend un événement que doit produire un autre.

!!! example "Exemple : l'accès ressource"
    === "2 processus pour 2 ressources"
        _Certaines ressources ont un accès exclusif : un seul accès pour un seul processus._
        Deux processus sont créés : P1 et P2.  
        Deux ressources R1 et R2 sont libres.
    
        |Processus P1|Processus P2|
        |:-:|:-:|
        |P1 est **éligible**|P2 est **éligible**|
        |P1 devient l'**élu** : il est exécuté.||
        |P1 demande l'accès à R1 : il est donc arrêté dans son exécution et passe **bloqué**.|P2 prend alors la place d'**élu** : il est exécuté|
        ||P2 demande l'accès à R2 : il passe **bloqué**|
        |P1 obtient R1 et passe **éligible**|P2 obtient R2 : **éligible**|
        |P1 passe **élu**||
        |P1 demande l'accès à R2 **mais ne libère pas R1**||
        |P1 devient **bloqué** (R2 n'est pas libre)|P2 passe **élu**
        ||P2 demande l'accès à R1 : il passe **bloqué**|
        |**Bilan pour P1 : Bloqué**|**Bilan pour P2 : Bloqué**| 
        |P1 veut R2|P2 veut R1|

    === "4 voitures pour un carrefour"
        Chaque voiture a la priorité à droite.
        
        ![carrefour](images/carrefour.png)

___
## Processus et Système d'exploitation (S.E.)

!!! info inline end "Remarques sur l'identité"

    Intérêt de séparer l'utilisateur propriétaire de l'utilisateur effectif :

    - Droits d'administration « locale » : certaines commandes, au comportement connu et circonscrit, s'exécutent avec des droits supérieurs à l'utilisateur qui les lancent. (exemple : ``sudo``)
    - Services lancés par l'administrateur  : pour limiter les risques en cas d'exploitation de faille, ils s'éxécutent avec des droits limités.

Les processus sont gérés par le système d'exploitation. Pour cela, ce dernier détient des informations sur chaque processus :

- Identificateur de processus (PID) : numéro unique (à un instant *t*, mais réutilisable)
- Informations de généalogie : processus parent (PPID), processus enfants
- Information de droits : utilisateur propriétaire du processus, utilisateur effectif (augmentation ou diminution de droits)

Plusieurs processus indépendants doivent pouvoir s'exécuter sans interférence :

- partage des ressources (arbitrage)
- isolation des processus (abstraction, autorisation)

À l'inverse, les processus doivent pouvoir communiquer si leur programme le demande

- IPC (communication inter-processus)
- Informations sur la mémoire utilisée / utilisable
- Informations sur le temps passé (temps réel, temps utilisateur)

- Liste des fichiers ouverts (fichiers au sens large)

- Autres ressources utilisées...

Le S.E. a un service d'ordonnancement : choix du processus à élire, temps alloué à son exécution par le processeur


___
## Ordonnancement de processus

Les objectifs sont multiples :

- assurer que chaque processus éligibles puissent s'exécuter
- minimiser le temps de réponse
- utiliser le maximum de capacité du processeur
- équilibrer les ressources
- prendre en compte les priorités
- prévoir la suite possible des évênements
- ...

!!! info "Stratégies typiques"

    === "FIFO sans préemption"

        - Premier arrivé, premier servi

        - Ordonnancement **coopératif** (pas de préemption) : les processus rendent la main « de leur plein gré »,  
            - lorsqu'ils se terminent,
            - lorsqu'ils se bloquent, 
            - lorsqu'ils font l'appel système

        === "inconvénients"
            - temps de réponse dépend du processus qui a la main
            - tant qu'il ne rend pas la main, les autres doivent attendre
            - pénalise les processus courts
            - proportion temps d'attente / temps d'exécution

        === "avantages"
            - simple
            - surcoût faible
            - équitable

        === "adapté pour"
            - nombreux cœurs de calcul
            - processus très fréquemment bloqués (gestions E/S)
            - machine peu puissante, ou le surcoût doit être minimisé

        === "utilisations"
            - certains systèmes batch
            - faible surcoût, meilleure utilisation du/des processeur(s)  
            - pas de contrainte de temps de réponse
            - Mac OS  X (multitâche coopératif)
            - faible surcoût, intéressant pour une machine lente  
            - processus « temps réel » sous Linux
            - processus de gestion des E/S, fréquemment bloqués mais nécessitant une réaction très rapide aux interruptions matérielles


    === "Tourniquet"
        _(En anglais : *round robin*, ruban rond)_

        - FIFO avec préemption  
        - Chaque processus reçoit un **quantum** de temps  
        - Une fois le quantum épuisé, le processus passe la main au retourne dans la file d'attente  


    === "Shortest Job Next"
        - On donne toujours la main à celui qui va mettre le moins de temps avant de se bloquer / terminer
        - Cela suppose d'avoir une connaissance / estimation de ce temps pour chaque processus : hypothèse forte

        === "Avantages"
            maximise le temps de réponse, le débit (nombre de processus terminés par unité de temps)

        === "Inconvénients"
            - surcoût
            - inéquitable, famine possible (processus calculatoires)


    === "Highest Response Ratio Next"
        - Variante de la stratégie précédente : on prend en compte le ratio du temps que le processus a passé à attendre, sur le temps dont il a besoin

        - Supprime le problème de famine (plus un processus attend, plus il augmente ses chances d'obtenir la main)

        - Mais suppose toujours la connaissance du temps d'exécution


!!! info "Stratégies de priorité"

    - Certaines stratégies donnent la priorité à certains types de processus (ex: SJN, HRRN, ML Feedback)

    - On souhaite aussi donner à l'utilisateur la possibilité d'influer sur la priorité d'un processus

    - Distinction entre :

        - priorité externe : propriété définie par l'utilisateur, variant peu
        - priorité interne : propriété gérée par l'ordonnanceur, variant plus souvent
        
        _Exemple sous UNIX: la commande `nice` permet de modifier la priorité externe_

    - Influence de la priorité externe:

        - en fixant leur priorité interne de départ
        - en limitant la plage de priorité interne dans laquelle ils peuvent évoluer

    - Les systèmes modernes possèdent plusieurs classes de priorité, hermétiques entre elles, et gérées différemment


    === "Partage équitable"
        _(En anglais *fair share*)_
    
        - Le processus n'est pas forcément le bon grain pour mesurer l'équité d'un ordonnanceur

        - Dans certain cas, on peut souhaiter partager le temps processeur équitablement entre :

            - les utilisateurs : indépendamment du nombre de processus lancé par chacun d'eux

            - des groupes d'utilisateurs : indépendamment du nombre d'utilisateur et de processus


    === "Temps réel"

        - Objectif : garantir autant que possible certains délais aux processus

        - Inconvénient : le système doit faire des estimations « dans le pire des cas » : dégrade les performances (temps de réponse, débit)

        - Exemple de stratégie : EDF (*Earliest Deadline First*)

            Optimal lorsqu'il est possible de respecter tous les délais...  
            ...mais très mauvais lorsque le système est surchargé



        !!! example "Exemples réels"

            === "Linux"
                Trois classes de priorité, chacune comportant plusieurs niveaux de priorité
                    
                - « Temps réel » FIFO : une FIFO par niveau de priorité

                - « Temps réel » tourniquet : un tourniquet par niveau de priorité
                - Autre : ML Feedback « amélioré » (priorité utilisateur)

            === "Windows"

                32 niveaux de priorité, divisés en deux classes
                
                - « Temps réel » (16-31) : priorité fixe, chaque niveau géré par un tourniquet

                - « Variable » (0-15)

                    - les processus migrent d'un niveau à l'autre en fonction de la consommation de leur quantum, et du type d'E/S qu'ils effectuent

                    - favorise les processus interactifs (E/S clavier, souris, écan...)

                Sur un système à N processeurs

                - les N-1 processus les plus prioritaires ont chacun un processeur

                - tous les autres processus se partagent le dernier processeur

            === "Ordonnanceur « applicatif »"

                 Exemple : serveur HTTP, gérant de nombreuses connexions simultanées:

                 - utiliser un processus ou un *thread* par connexion, en laissant le système d'exploitation gérer l'ordonnancement.

                 - utiliser un nombre fixe de *threads* (potentiellement un seul), en gérant « à la main » l'ordonnancement *entre les connexions*.
