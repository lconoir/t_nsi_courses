#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
:Communication réseau
:Module serveur
:Auteur : Conoir Lionnel
:Date : avril 2021
'''


import socket, sys

HOST = '192.168.1.2'           # adresse du serveur
PORT = 50000                   # port de communication utilisé pour la connexion au serveur
     
# création du socket : interface de connexion
interface = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
     
# liaison du socket à une adresse précise :
try:
    interface.bind((HOST, PORT))
except socket.error:
    print("La liaison du socket à l'adresse choisie a échoué.")
    sys.exit()

serveur_ecoute = True

while serveur_ecoute:
    
    # attente de la requête de connexion d'un client :
    print("Serveur prêt, en attente de requêtes ...")
    interface.listen(1)                                  # seulement un client est attendu
    
    # acceptation de la connexion d'un client :
    connexion, adresse = interface.accept()
    print("Client connecté, adresse IP %s, port %s" % (adresse[0], adresse[1]))
    
    # message de connexion au client :
    message_serveur = "Vous êtes connecté.\n         Envoyez vos messages."   # premier message du serveur
    connexion.send(message_serveur.encode("Utf8"))            # envoi du message
    
    # dialogue avec le client :
    dialogue = True
    
    while dialogue:
        message_client = connexion.recv(1024).decode("Utf8")  # message reçu
        print("Client>", message_client)                      # message affiché
        
        if message_client.upper() == "FIN" :
            dialogue = False
        else :
            message_serveur = input("Serveur> ")              # le serveur écrit un message
            if message_serveur == '':
                message_serveur = ' '                         # le message ne doit pas être vide
            connexion.send(message_serveur.encode("Utf8"))    # message envoyé au serveur
            
    # fermeture de la connexion :
    print("Connexion interrompue.")
    connexion.close()
    
    suite = input("<R>ecommencer <T>erminer ? ")
    if suite.upper() =='T':
        serveur_ecoute = False