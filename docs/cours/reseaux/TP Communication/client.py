#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
:Communication réseau
:Module client
:Auteur : Conoir Lionnel
:Date : avril 2021
'''


import socket, sys, time

HOST = '192.168.1.2'           # adresse du serveur
PORT = 50000                   # port de communication utilisé pour la connexion au serveur

# création du socket : interface de connexion
interface = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# envoi d'une requête de connexion au serveur
connexion = False

while connexion == False:

    try:
        interface.connect((HOST, PORT))
        connexion = True
    except socket.error:
        print(".", end = '')     # le serveur est éteint ou a un problème
        time.sleep(2)
    

# dialogue avec le serveur :
dialogue = True

while dialogue :
    message_serveur = interface.recv(1024).decode("Utf8")  # message reçu
    print("Serveur>", message_serveur)                     # message affiché
    
    if message_serveur.upper() == "FIN":                   # dialogue terminé
        dialogue = False
    else :
        message_client = input("Client> ")                 # le client écrit un message
        if message_client == '':
                message_client = ' '                       # le message envoyé ne doit pas être vide
        interface.send(message_client.encode("Utf8"))      # message envoyé au serveur
        
# fermeture de la connexion :
print("Connexion interrompue.")
interface.close()