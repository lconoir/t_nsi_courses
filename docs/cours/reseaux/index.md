___
Un réseau informatique est un ensemble de machines interconnectées (par fils ou ondes)

Les grands réseaux (comme internet) sont découpés en sous-réseaux.


## Protocoles TCP-IP

Chaque machine d'un sous-réseau est repérée par une adresse I.P. (automatiquement attribuée ou fixée par l'administrateur).

Les ordinateurs et autres interfaces matérielles (imprimantes) sont connectées à un **commutateur** (switch en anglais).

Chaque sous-réseau possède un masque sous-réseau permettant de fixé l'adressage I.P. des machines.

Les communications entre les machines s'effectuent par transmissions de paquets numériques. Le protocole TCP contrôle et gère l'acheminement de ces paquets.

Des machines, appelées routeurs, établissent les liens entre les sous-réseaux.

Il est néanmoins nécessaire d'avoir aussi recours à des protocoles de routage permettant faire circuler des paquets d’un point du réseau à un autre.

___
## La table de routage

Un routeur ou un PC s’appuie sur sa table de routage.

Elle se compose généralement de 5 éléments :
    • Le réseau que l’on souhaite atteindre
    • Le masque du sous-réseau à atteindre
    • Le chemin à emprunter : le routeur suivant qui permet de rejoindre la destination et par quelle interface l’atteindre
    • La métrique : distance ou coût
     
Et si la route n’existe pas, on attribue une route par défaut.



___
## Protocoles R.I.P.

C'est un routage à vecteur de distance dans lequel la distance (ou coût) est le nombre de sauts pour y aller. 


Basé sur l'algorithme de Bellman-Ford :

Périodiquement (30 s), chaque routeur envoie une copie de sa table de routage à tous les routeurs voisins (directement accessibles).

Lorsque qu’un routeur A reçoit une table d’un voisin B :
	pour chaque destination affichée avec la distance associée, le routeur A  modifie sa table si : 
		B annonce une destination que A ne possède pas :
			nouvelle ligne dans la table
		B connaît un plus court chemin :
			la passerelle et la distance, mises à jour pour la destination
		la destination passant par B a changée
			la distance est mise à jour

NB : la distance à partir de A = distance à partir de B + 1


RIP v1 :  
Si au bout de 3 minutes, un réseau n’est plus annoncé, il est supprimé de la table.  
Il n’y a pas d’accusé de réception.  
Un routeur ne stocke que 25 entrées maximum.  
Le nombre de saut est limité à 15 : la valeur 16 est considéré comme inaccessible.

RIP v2 :  
Les données ne sont pas renvoyées vers le routeur où on les a prises.  
Si une route est coupé et qu’un message renvoie un coût très supérieur au coût initial, l’information est ignorée.





___
## Protocoles O.S.P.F.
