# -*- coding: utf-8 -*-
'''
:Titre : Validation d'un mot de passe
:Auteur : L. Conoir
:Date : 09/2020
'''


from password import validation_mot_de_passe

print('''Choisir un mot de passe répondant à ces critères minimum :
    3 majuscules
    3 minuscules
    2 chiffres
    2 caractères spéciaux
    longueur de 12 caractères\n''')


valide = False

while not valide :
    
    reponse = input('Définir votre mot de passe : ')
    
    if not validation_mot_de_passe(reponse) :
    
        reponse2 = input('Retaper votre mot de passe (vérification) : ')
        
        if reponse != reponse2 :
            print('Les mots tapés sont différents. Veuillez recommencer.\n')
        else :
            valide = True

print('\nMot de passe validé et enregistré.')