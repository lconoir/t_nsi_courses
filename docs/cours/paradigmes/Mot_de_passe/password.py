# -*- coding: utf-8 -*-
'''
:Titre : Validation d'un mot de passe
:Auteur : L. Conoir
:Date : 09/2020

Critères minimums définis :
  3 majuscules
  3 minuscules
  2 chiffres
  2 caractères spéciaux
  longueur de 12 caractères
'''

########################################################
MINUSCULES = 'abcdefghijklmnopqrstuvwxyz'
MAJUSCULES = MINUSCULES.upper()
CHIFFRES = '0123456789'
ALPHANUM = MAJUSCULES + MINUSCULES + CHIFFRES
NB_CARACTERES = 'Nombre total de caractères'

donnees = {MINUSCULES : 3,
           MAJUSCULES : 3,
           CHIFFRES : 2,
           ALPHANUM : 2,
           NB_CARACTERES : 12}

########################################################
def verif(mot, critere, contient = True) :
    '''
:examples:
>>> donnees = {MINUSCULES : 3, MAJUSCULES : 3, CHIFFRES : 2, ALPHANUM : 2, NB_CARACTERES : 12}
>>> verif('abcdDEF012&~#', MINUSCULES)
True
>>> verif('abcDEF012&~#', MAJUSCULES)
True
>>> verif('abDEF012&~#', MINUSCULES)
False
>>> verif('abcDEF012&~#', CHIFFRES)
True
>>> verif('abcDEF012&', ALPHANUM, False)
False
>>> verif('abcDEF012&~#', ALPHANUM, False)
True
>>> verif('abcDEF012&~#', ALPHANUM)
True
'''
    nombre = 0
    
    if contient :
        for caractere in mot :
            if caractere in critere :
                nombre = nombre + 1
            if nombre == donnees[critere] :
                return True
            
    else :
        for caractere in mot :
            if caractere not in critere :
                nombre = nombre + 1
            if nombre == donnees[critere] :
                return True
            
    return False


########################################################
def verif_majuscules(mot) :
    '''Précise si le nombre de majuscules est respecté dans le mot.
:param: mot, type str, mot de passe testé
:return: type bool, True si le critère est vérifié, sinon False
:CU: mot est de type str
:bord_effect: None'''
    return verif(mot, MAJUSCULES)


########################################################
def verif_minuscules(mot) :
    '''Précise si le nombre de minuscules est respecté dans le mot.
:param: mot, type str, mot de passe testé
:return: type bool, True si le critère est vérifié, sinon False
:CU: mot est de type str
:bord_effect: None'''
    return verif(mot, MINUSCULES)


########################################################
def verif_chiffres(mot) :
    '''Précise si le nombre de chiffres est respecté dans le mot.
:param: mot, type str, mot de passe testé
:return: type bool, True si le critère est vérifié, sinon False
:CU: mot est de type str
:bord_effect: None'''
    return verif(mot, CHIFFRES)


########################################################
def verif_speciaux(mot) :
    '''Précise si le nombre de caractères spéciaux est respecté dans le mot.
:param: mot, type str, mot de passe testé
:return: type bool, True si le critère est vérifié, sinon False
:CU: mot est de type str
:bord_effect: None'''
    return verif(mot, ALPHANUM, False)


########################################################
def verif_total_caract(mot) :
    '''Précise si le nombre total de caractères est respecté dans le mot.
:param: mot, type str, mot de passe testé
:return: type bool, True si le critère est vérifié, sinon False
:CU: mot est de type str
:bord_effect: None'''
    return len(mot) == donnees[NB_CARACTERES]


########################################################
def validation_mot_de_passe(mot) :
    '''Précise si tous les critères sont respectés dans le mot.
:param: mot, type str, mot de passe testé
:return: type bool, True si tous les critères sont vérifiés, sinon False
:CU: mot est de type str
:bord_effect: None'''
#    assert isinstance(mot, str), 'Le paramètre de la fonction doit être de type str.'
    try :
        if not verif_total_caract(mot) or not verif_speciaux(mot) or not verif_majuscules(mot) or not verif_minuscules(mot) or not verif_chiffres(mot)  :
            return False
        return True
    except TypeError :
        print('Le paramètre de la fonction doit être de type str.')
    

########################################################
if __name__ == '__main__' :
    import doctest
    doctest.testmod()