# -*- coding: utf-8 -*-
'''
:Titre : Module de classe Joueur()
:Auteur : L. Conoir
:Date : 05/2020
'''

class Joueur() :
    '''Classe définissant un joueur à partir de son nom.

Un objet, instance de cette classe, possède plusieurs méthodes :

    GetNom() : renvoie le nom de l'objet
    SetNouveauCoup(coup) : affecte le 'coup' joué à la liste des coups joués
    GetCouleur : renvoie la couleur de l'objet
    SetCouleur : affecte la couleur de l'objet
    GetPosition : renvoie la position de l'objet, tuple (x, y)
    Avance() : l'objet avance
    Recule() : l'objet recule
    TourneGauche() : l'objet tourne à gauche
    TourneDroite() : l'objet tourne à droite'''

    
    def __init__(self, nom):
        '''Méthode dédiée, constructeur de la classe'''
        
        self.__nom = nom
        self.__statut = 'Débutant'
        self.__coups = []
        self.__points = 0
        self.__parties_jouees = 0
        self.__parties_gagnees = 0
        
    def GetNom(self):
        '''Méthode publique, renvoie le nom du joueur.'''
        return self.__nom
            
    def SetNouveauCoup(self, coup):
        '''Méthode publique, affecte le coup à la liste des coups.'''
        self.__coups.append(coup)
    
    def GetCoups(self):
        '''Méthode publique, renvoie la liste des coups joués.'''
        return self.__coups
        
    def GetPoints(self):
        '''Méthode publique, renvoie le nombre de points du joueur'''
        return self.__points
    
    def __PartieJouee(self):
        '''Méthode privée, augmente le nombre de parties jouées de 1.'''
        self.__parties_jouees = self.__parties_jouees + 1
        
    def GetPartiesJouees(self):
        '''Méthode publique, renvoie le nombre de parties jouées.'''
        return self.__parties_jouees
    
    def __PartieGagnee(self):
        '''Méthode privée, augmente le nombre de parties gagnées de 1.'''
        self.__parties_gagnees = self.__parties_gagnees + 1
        
    def GetPartiesGagnees(self):
        '''Méthode publique, renvoie le nombre de parties gagnées.'''
        return self.__parties_gagnees
    
    def Fin_de_partie(self, resultat):
        '''Méthode publique, affecte '''
        self.__PartieJouee()
        self.__coups = []
        if resultat == 'Gagnée':
            self.__PartieGagnee()
        
    
    def __str__(self):
        '''Méthode dédiée, affiche des données de l'objet.'''
        affichage = 'Nom : ' + self.GetNom() + '\nNombre de parties gagnées : ' + self.GetPartiesGagnees() + ' sur ' + self.GetPartiesJouees()
        return affichage