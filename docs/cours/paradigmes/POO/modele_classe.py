# -*- coding: utf-8 -*-
'''
:Titre : Modèle de classe
:Auteur : L. Conoir
:Date : 05/2020
'''

class Nom_de_la_classe(object) :
    '''Docstring'''
    
    def __init__(self, parametres_nécessaires_pour_construire):
        '''Méthode dédiée, constructeur de la classe'''
        self.__attribut1 = valeur1
        self.__attribut2 = valeur2
        self.__attribut3 = valeur3
        self.__attribut4 = valeur4
        ...
        
    def Methode1(self, parametres):
        '''Méthode publique'''
        return ........
            
    def __Methode2(self, parametres):
        '''Méthode privée'''
        return ........
   
    