# -*- coding: utf-8 -*-
'''
:Titre : Utilisation de la classe Vehicule()
:Auteur : L. Conoir
:Date : 05/2020
'''

from usine_vehicule import Vehicule

polo = Vehicule('Voiture', 'blanc')
char_leclerc = Vehicule('Char', 'vert kaki')
biclou = Vehicule('Vélo', 'crade')
trote = Vehicule('Trotinette', 'gris')


print(polo)
print(char_leclerc)
# print()
# print(type(polo))
# print()
# print(polo.__doc__)
# print()
# print(polo.__init__.__doc__)
# print()
# print(polo.GetCouleur.__doc__)
