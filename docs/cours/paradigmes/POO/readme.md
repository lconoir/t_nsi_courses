La POO a fait ses début dans les années 1960 avec le langage Lisp, mais s'est confirmée dans les langages Simula, puis SmallTalk (1970).
Elles s'est développée dans les langages plus anciens comme le Fortran, le Cobol et le C (devenu C++)
L'idée est de représenter un objet réel, avec ses caractéristiques et son comportement.
En analysant l'objet, on fait la liste de ses **attributs** (ceux qui nous intéressent) qui permettront de connaître son état ou que l'on peut ajouter par besoin.
On s'intéresse ensuite aux actions qu'il peut faire ou que l'on peut faire avec lui : ce sont les **méthodes**.

# Objet

L'idée est de représenter un objet réel (ou pas), avec ses caractéristiques et son comportement.
Exemple 1 : un livre

- Attributs : titre, auteur, éditeur, nombre de pages, page actuelle de lecture, ...
- Méthodes : ouvrir, fermer,  ranger, …

Exemple 2 : un personnage

- Attributs : nom, taille, force, tribu, amis, ennemis,  …..
- Méthodes : avancer, reculer, tourner, courir, sauter, manger, …

# Classe

Une **classe** est un type objet, une catégorie d’objets. Elle permet de créer des objets : valeurs du type de cette classe.
On appelle **instance** d’une classe, un objet créé par cette classe.
Un **attribut** est une donnée qui appartient à un objet et représente un état de cet objet : il est défini par la classe de l'objet.

Une **méthode** est une fonction qui appartient à une classe : elle ne peut être utilisée que par les instances de la classe qui la définit.

# Création d'une classe

Conceptuellement, les attributs sont définis **privé** mais modifiables (ou pas) par l’utilisateur.

Le développeur décide aussi de rendre **privé** ou **publique** les méthodes.
   - privé : l’utilisateur ne voit pas et ne peut donc utiliser
   - publique : l’utilisateur voit et peut utiliser

En rendant privé ou publique et en définissant les méthodes, le développeur établit **l’interface** de la classe : champ d’action de l’utilisateur.

De ce fait, les données internes et méthodes privés de chaque objet sont ainsi comme enfermées dans l’objet : principe de **l’encapsulation**.

La structure de définition d’une classe est similaire à celle d’une fonction mais comporte des éléments spécifiques : 

````python
--8<--- "docs/Cours/paradigmes/poo/modele_classe.py"
````

