# -*- coding: utf-8 -*-
'''
:Titre : Module de classe Vehicule()
:Auteur : L. Conoir
:Date : 05/2020
'''

class Vehicule(object) :
    '''Classe définissant un véhicule à partir de son style (voiture, bus, camion, moto...)
et de sa couleur.

Un objet, instance de cette classe, possède plusieurs méthodes :

    GetStyle : renvoie le style de l'objet
    GetCouleur : renvoie la couleur de l'objet
    SetCouleur : affecte la couleur de l'objet
    GetPosition : renvoie la position de l'objet, tuple (x, y)
    Avance() : l'objet avance
    Recule() : l'objet recule
    TourneGauche() : l'objet tourne à gauche
    TourneDroite() : l'objet tourne à droite'''

    
    def __init__(self, style, couleur = 'Non définie'):
        '''Méthode dédiée, constructeur de la classe'''
        
        from random import randint
        
        self.__style = style
        self.__couleur = couleur.capitalize()
        self.__position = randint(0, 40), randint(0, 40)
        self.__orientation = 'D'
        self.__directions_possibles = {'D' : (1, 0),
                                       'G' : (-1, 0),
                                       'H' : (0, -1),
                                       'B' : (0, 1)}
        
    def GetStyle(self):
        '''Méthode publique, renvoie le style de l'objet.'''
        return self.__style
            
    def GetCouleur(self):
        '''Méthode publique, renvoie la couleur de l'objet.'''
        return self.__couleur
    
    def SetCouleur(self, couleur):
        '''Méthode publique, affecte la couleur de l'objet.'''
        self.__couleur = couleur
        
    def GetPosition(self):
        '''Méthode publique, renvoie la position de l'objet : tuple (x, y)'''
        return self.__position
    
    def __SetPosition(self, position):
        '''Méthode privée, affecte la position de l'objet : tuple (x, y)'''
        self.__position = position
        
    def __GetOrientation(self):
        '''Méthode privée, renvoie l'orientation de l'objet.'''
        return self.__direction
    
    def __SetOrientation(self, direction):
        '''Méthode privée, affecte l'orientation de l'objet : 'H', 'B', 'G' ou 'D'.'''
        self.__direction = direction
    
    def Avance(self):
        '''Fait avancer la voiture d'une case.'''
        x, y = self.GetPosition()
        dx, dy = self.__directions_possibles[self.__GetDirection()]
        self.__SetPosition((x + dx, y + dy))
        
    def Recule(self):
        '''Fait reculer la voiture d'une case.'''
        x, y = self.GetPosition()
        dx, dy = self.__directions_possibles[self.__GetDirection()]
        self.__SetPosition((x - dx, y - dy))
        
    def TourneGauche(self):
        '''Fait tourner la voiture à gauche.'''
        change = {'H' : 'G',
                  'G' : 'B',
                  'B' : 'D',
                  'D' : 'H'}
        self.__SetDirection(change[self.__GetDirection()])
        self.Avance()
        
    def TourneDroite(self):
        '''Fait tourner la voiture à droite.'''
        change = {'H' : 'D',
                  'G' : 'H',
                  'B' : 'G',
                  'D' : 'B'}
        self.__SetDirection(change[self.__GetDirection()])
        self.Avance()
        
    def __str__(self):
        '''Méthode dédiée, affiche des données de l'objet.'''
        affichage = 'Nom : ' + '\nStyle : ' + self.GetStyle() + '\nCouleur : ' + self.GetCouleur()
        x, y = self.GetPosition()
        affichage = affichage + '\nPosition :\n   x = ' + str(x) +'\n   y = ' + str(y) + '\n' 
        return affichage