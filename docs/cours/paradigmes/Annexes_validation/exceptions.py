# -*- coding: utf-8 -*-
'''
:Titre : Validation d'un problème
:Auteur : L. Conoir
:Date : 09/2020
'''

def division(a, b):
    '''Donne le résultat de la division de a par b
:param: a,b, type int ou float, nombres
:return: type int
:CU: types de a et b respectés, b non nul
:examples:
>>> division(2, 3)
0
>>> division(7, 3.2)
2'''
    try :
        return int(a // b)
    
    except ZeroDivisionError :
        print('La division par 0 est impossible.')
    
    except TypeError :
        print('La division avec ce type de valeur est impossible.')
    




##################################################################################################################
if __name__ == "__main__":
    import doctest
    doctest.testmod()