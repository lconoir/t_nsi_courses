#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Titre : Complexité temporelle de tri
:author: L. Conoir
:date: juin 2020

Quatre algorithmes de tris :

* tri sélection : tri_1
* tri insertion : tri_2
* tri à bulles : tri_3
* tri rapide : tri_4


"""
from timeit import timeit
import pylab


#######################################################################
############# MESURE DU TEMPS POUR UNE METHODE DE TRI #################
#######################################################################
def temps_tri(methode_tri, n) :
    """Mesure du temps d'exécution de la méthode de tri choisie
pour le tri de 100 listes de n nombres mélangés.
:param: n, type int(), effectif d'item dans une liste
:return: type list(), liste des temps calculés pour chaque liste
:CU: n entier positif
:effet de bord: None
"""
    resultat = []
    
    for i in range(n) :
        
        tri = methode_tri + '(cree_liste_melangee(' + str(i+1) + '))'
        
        resultat.append(timeit(setup = 'from differents_tris import ' + methode_tri + ', cree_liste_melangee', 
                               stmt = tri,
                               number = 100))
    
    return resultat

#######################################################################
################# COURBE DE TEMPS POUR UNE METHODE DE TRI #############
#######################################################################
def courbe_tri(methode_tri, n) :
    """Tracé de la courbe de temps d'exécution de la méthode de tri choisie
pour le tri de 100 listes de 1 à n nombres mélangés
:param: type int(), effectif maximal d'item dans une liste
:return: None
:CU: n entier positif
:effet de bord: None
"""
    NBRE_ESSAIS = 100
    titre = 'Temps du tri avec la fonction ' + methode_tri + '(pour ' + str(NBRE_ESSAIS) + ' essais.'
    pylab.title(titre)
    pylab.xlabel('taille des listes')
    pylab.ylabel('temps en secondes')
    pylab.grid()
    pylab.plot(range(1,n+1),temps_tri(methode_tri, n))
    pylab.show()
