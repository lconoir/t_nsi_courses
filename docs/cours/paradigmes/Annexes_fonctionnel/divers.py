# -*- coding: utf-8 -*-
'''
:Titre : Fonctions récursives
:Auteur : L. Conoir
:Date : 09/2020

'''

def maximum(liste):

    if len(liste) == 1:
        return liste[0]

    milieu = len(liste)//2
    max1 = maximum(liste[:milieu])
    max2 = maximum(liste[milieu:])

    if max1 > max2:
        return max1

    return max2


def pair(N):
    if N == 1:
        return False
    return impair(N-1)


def impair(N):
    if N == 1:
        return True
    return pair(N-1)

