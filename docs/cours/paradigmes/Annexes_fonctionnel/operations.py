# -*- coding: utf-8 -*-
'''
:Titre : Module d'opérations
:Auteur : L. Conoir
:Date : 09/2020
'''

ZERO = 0

def addition(a, b) :
    return a + b

def soustraction(a, b) :
    return a - b

def multiplication(a, b) :
    if a == 0 or b == 0 :
        return 0
    if a == 1 :
        return b
    return addition(b, multiplication(a - 1, b))

def division(a, b) :
    assert b != 0, 'Division par 0 impossible' 
    return a // b, a % b

