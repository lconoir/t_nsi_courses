Plusieurs langages existent et sont d'ailleurs difficilement dénombrables (env 2500 depuis 1950).

|Quelques uns|   |   |   |   |   |
|---|---|---|---|---|---|
|ABC|Ada|Algol|AWK|APL|B|
|Basic|C|C++|C#|CAML|CLU|
|Cobol|CPL|CSS|Dart|Delphi|Eiffel|
|Flow-Matic|Forth|Fortran|Go|Hack|Haskell|
|HTML|ICon|J|Java|Javascript|Julia|
|Kotlin|Lisp|Mainsail|M|ML|Modula|
|Oberon|Objective-C|OCaml|Pascal|Perl|PHP|
|PL/I|Postscript|Prolog|Python|R|Ruby|
|Rust|Scade|Scala|Scheme|Sh|Simula|
|SmallTalk|SQL|Swift|Tcl/TK|TypeScript|VBA|

Ils sont souvent classés par catégories : **paradigmes de programmation**.

Un paradigme est une façon de coder, mettant en avant la vision propre de la résolution d'un problème par le développeur.  
Certains problèmes se traitent plus facilement selon un certain paradigme et donc le choix du langage se fait selon la nature du problème à traiter.  
Un langage peut s'articuler avec un seul ou plusieurs paradigmes.  

___
## Paradigme impératif

C'est le plus ancien, inventé à la création des premiers ordinateurs. Il repose sur ces notions :
    • la séquence d'instructions (les instructions sont exécutées l'une après l'autre) 
    • l'affectation (on attribue une valeur à une variable) 
    • l'instruction conditionnelle
    • la boucle

!!! example "Quelques exemples"
    C, Cobol, Fortran, Basic, Pascal, Python ...

___
## Paradigme fonctionnel

On utilise des fonctions acceptant un ou plusieurs paramètres et produisant une ou des valeurs, Dans un langage purement fonctionnel, il n'y a pas d'effet de bord, car la notion de variables n'existe pas.  
Une fonction peut être primaire ou composée d'autres fonctions.  

!!! example "Quelques exemples"
    Lisp, LOGO, OCaml, Haskell, Python ...

___
##  Paradigme orienté objet (POO)

La première idée est de représenter, recréer, des objets réels (ou pas).  
Les objets construits font alors faire partie d'une catégorie d'objets (une classe) regroupant des aspects communs. On cherche ensuite à décrire les échanges qu'ils pourront avoir, entre eux ou avec l'extérieur, et produire ainsi des comportements voulus (méthodes).  
Ce paradigme permet ainsi une abstraction très importante et constitue un excellent outil de modularité.  


!!! example "Quelques exemples"
    SmallTalk, C++, Java, Python...

D'autres paradigmes existent : événementiel, concurrent, orienté contraintes, orienté requêtes, synchrone, logique, modélisation...

